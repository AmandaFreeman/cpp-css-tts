chrome.runtime.sendMessage({todo: "showPageAction"});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (request.get === "getText"){
        let element = $('#left').html();
        let elementWithoutBigTags = element.replace(/<big>.+<\/big>/, "");
        text = $(elementWithoutBigTags).text().replace(/\S+\.cpp\s+?$|source_\S+|\s\S+\.h\s+?$/, "");
        sendResponse({textContent: text});
    };
});