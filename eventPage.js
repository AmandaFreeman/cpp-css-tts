var RATE = 1.8;

// use previously set speech rate if available
chrome.storage.sync.get(['rate'], function(result) {
    if (typeof result.rate !== undefined) RATE = result.rate;
});

// show page on https://edu.openedg.org/*
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.todo === "showPageAction") {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
            chrome.pageAction.show(tabs[0].id);
        });
    }
});

// listen for keyboard shortcut for command 'read', get text and read it
chrome.commands.onCommand.addListener(function(command) {
    chrome.tts.stop();
    if (command === 'read') {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {get: "getText"}, function(response) {
                const text = response && response.textContent ? response.textContent : "";
                if (text) chrome.tts.speak(text, {rate: RATE});
            });
        });
    }
});

// context menu options
const speak_current_speed = {
    "id": "speak_current_speed",
    "title": "Speak",
    "contexts": ["page", "selection"]
};

const stop = {
    "id": "Stop",
    "title": "Stop Speaking",
    "contexts": ["page", "selection"]
};

const speakRate_1_2 = {
    "id": "speakRate_1_2",
    "title": "Speak Slow",
    "contexts": ["page", "selection"]
};

const speakRate_1_4 = {
    "id": "speakRate_1_4",
    "title": "Speak Normal",
    "contexts": ["page", "selection"]
};

const speakRate_1_8 = {
    "id": "speakRate_1_8",
    "title": "Speak Fast",
    "contexts": ["page", "selection"]
};

// ************************************************************************************
// listen for context menu clicks and perform actions to suit
chrome.contextMenus.create(speak_current_speed, () => chrome.runtime.lastError);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "speak_current_speed"){
        chrome.tts.stop();
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {get: "getText"}, function(response) {
                const text = response.textContent;
                if (text) chrome.tts.speak(text, {rate: RATE});
            });
        });
    }
});

// ************************************************************************************

chrome.contextMenus.create(speakRate_1_2, () => chrome.runtime.lastError);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "speakRate_1_2"){
        chrome.tts.stop();
        RATE = 1.2;
        chrome.storage.sync.set({rate: RATE});
        
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {get: "getText"}, function(response) {
                const text = response.textContent;
                if (text) chrome.tts.speak(text, {rate: RATE});
            });
        });
    }
});

// ************************************************************************************

chrome.contextMenus.create(speakRate_1_4, () => chrome.runtime.lastError);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "speakRate_1_4"){
        chrome.tts.stop();
        RATE = 1.4;
        chrome.storage.sync.set({rate: RATE});
        
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {get: "getText"}, function(response) {
                const text = response.textContent;
                if (text) chrome.tts.speak(text, {rate: RATE});
            });
        });
    }
});

// ************************************************************************************

chrome.contextMenus.create(speakRate_1_8, () => chrome.runtime.lastError);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "speakRate_1_8"){
        chrome.tts.stop();
        RATE = 1.8;
        chrome.storage.sync.set({rate: RATE});

        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {get: "getText"}, function(response) {
                const text = response.textContent;
                if (text) chrome.tts.speak(text, {rate: RATE});
            });
        });
    }
});

// ************************************************************************************

chrome.contextMenus.create(stop, () => chrome.runtime.lastError);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Stop"){
        chrome.tts.stop();
    }
});