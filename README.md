#+html: <p align="center"><img src="./images/cpp.png" /></p>

## Mac
This works well on a Mac with British English voice Kate as your native text-to-speech voice

You can set the native TTS voice in System Preferences > Accessibility > Speech


## Windows
I didn't have much luck with this on Windows 10. It seems to use some other voice than the native TTS one in the TTS settings

The speech rate doesn't change with the settings in the extension, it speaks quite slowly.

Aside from the voice issue it works on Windows. YMMV

# Installation
Clone the repository

Open Google Chrome and go here: chrome://extensions/

Turn on Developer Mode using the toggle button on the top right

Click the Load Unpacked button on the top left

Select the folder containing this repository

Checkout out this branch `better_colours_when_tts_keyboard_shortcut_pressed` for better colours. Currently they change when a keyboard shortcut is pressed as the colours reset on nextPage click(). When I find a better way, I'll update master with the better colours.

# Usage

Make sure the extension is enabled. This extension only works on the openedg url

Navigate to https://edu.openedg.org/

Sign in and open a chapter

The extension should work in the course content window

You can tell because the background will be blue and the text red

Right click on the left side of the page to find a context menu with TTS options

Start reading with `ctrl+a` and stop reading with `ctrl+s`

You can navigate the pages with the left and right arrow keys

# Customisation

For now if you want to change the colours you have to manually edit the content.css

If you want to disable the styling just comment out or delete the code in content.css

I might add a toggle button and possibly a colour picker in future

